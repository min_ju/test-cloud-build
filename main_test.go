package main

import "testing"

func TestEcho(t *testing.T) {
	t.Log("testing echo")
	expected := "hello world"
	actual := echo()
	if expected != actual {
		t.Errorf("Expected %s, but it was %s instead.", expected, actual)
	}
}

// BenchmarkEcho benchmark
func BenchmarkEcho(b *testing.B) {
	b.Log("benchmarking echo")
	for n := 0; n < b.N; n++ {
		echo()
	}
}
