FROM golang:1.11 as builder

ENV SRC_DIR=/go/src/bitbucket.org/min_ju/test-cloud-build
COPY . $SRC_DIR
WORKDIR $SRC_DIR
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

FROM scratch
ENV BUILDER_DIR=/go/src/bitbucket.org/min_ju/test-cloud-build
COPY --from=builder $BUILDER_DIR/main ./