package main

import (
	"fmt"
)

// just a simple function with a simple unit test in order to test cloud build
func main() {
	fmt.Println(echo())
}

// make everything simple
func echo() string {
	return "hello world"
}
